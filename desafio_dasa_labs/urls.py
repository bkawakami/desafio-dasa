"""desafio_dasa_labs URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^doctors/', include('doctors.urls')),
    url(r'^exams/', include('exams.urls')),
    url(r'^rooms/', include('rooms.urls')),
    url(r'^patient/', include('patients.urls')),
    url(r'^doc/', include('doc.urls')),
    url(r'^admin/', admin.site.urls),
]
