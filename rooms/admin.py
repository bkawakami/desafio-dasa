# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from rooms.models import Room, RoomSchedule


class RoomAdmin(admin.ModelAdmin):
    list_display = ['name', 'specialty', 'city', 'active']
    fields = ['name', 'specialty', 'city', 'active']
    search_fields = ['name', 'specialty', 'city', 'active']


class RoomScheduleAdmin(admin.ModelAdmin):
    list_display = ['room', 'exam', 'doctor', 'date_time']
    fields = ['room', 'exam', 'doctor', 'date_time']
    search_fields = ['room', 'exam', 'doctor', 'date_time']


admin.site.register(Room, RoomAdmin)
admin.site.register(RoomSchedule, RoomScheduleAdmin)
