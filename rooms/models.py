# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from doctors.models import Doctor
from exams.models import Exam


class Room(models.Model):
    name = models.CharField(max_length=512)
    specialty = models.CharField(max_length=512)
    city = models.CharField(max_length=512)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name


class RoomSchedule(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    exam = models.ForeignKey(Exam, on_delete=models.CASCADE)
    doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    date_time = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u"%s" % self.id
