# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse
from django.views import View
from django.core import serializers

# Create your views here.
from doctors.models import Doctor
from exams.models import Exam
from rooms.models import Room, RoomSchedule


class RoomApi(View):
    def get(self, request):
        records = Room.objects.filter()

        serialized = serializers.serialize('python', records,
                                           fields=['name', 'specialty', 'city', 'active'])

        json_record = [d['fields'] for d in serialized]

        data = {"status": "success", "records": json_record}

        return JsonResponse(data, status=200)


class RoomScheduleApi(View):
    def get(self, request):
        records = RoomSchedule.objects.filter()

        serialized = serializers.serialize('python', records,
                                                    fields=['room', 'exam', 'doctor', 'date_time'])

        json_record = [d['fields'] for d in serialized]

        for idx, item in enumerate(json_record):
            item['_id'] = serialized[idx]['pk']

            room = Room.objects.get(pk=item["room"])

            serialized_room = serializers.serialize('python', [room, ],
                                                    fields=['name', 'specialty', 'city', 'active'])

            room_record = [d['fields'] for d in serialized_room]

            doctor = Doctor.objects.get(pk=item["doctor"])

            serialized_doctor = serializers.serialize('python', [doctor, ],
                                                      fields=['name', 'specialty', 'city', 'active'])

            doctor_record = [d['fields'] for d in serialized_doctor]

            exam = Exam.objects.get(pk=item["exam"])

            serialized_exam = serializers.serialize('python', [exam, ],
                                                    fields=['name', 'specialty', 'time_for', 'active'])

            exam_record = [d['fields'] for d in serialized_exam]

            item['room_record'] = room_record
            item['doctor_record'] = doctor_record
            item['exam_record'] = exam_record

        data = {"status": "success", "records": json_record}

        return JsonResponse(data, status=200)
