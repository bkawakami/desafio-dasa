from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^all', views.RoomApi.as_view()),
    url(r'^schedule', views.RoomScheduleApi.as_view()),
]
