# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from doctors.models import Doctor


class DoctorAdmin(admin.ModelAdmin):
    list_display = ['name', 'specialty', 'city', 'active']
    fields = ['name', 'specialty', 'city', 'active']
    search_fields = ['name', 'specialty', 'city', 'active']


admin.site.register(Doctor, DoctorAdmin)
