# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Doctor(models.Model):
    name = models.CharField(max_length=512)
    specialty = models.CharField(max_length=512)
    city = models.CharField(max_length=512, null=True, blank=True)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name