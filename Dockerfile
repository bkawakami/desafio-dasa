FROM ubuntu:16.04

RUN apt -y update

RUN apt -y install python python-dev python-setuptools ca-certificates

# create workspace folder
RUN mkdir /code

# define as a workspace
WORKDIR /code

# setup to installation
ADD . /code/

RUN rm -rf venv

RUN python setup.py install

# RUN python manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'Adminpass$#@!5')"

EXPOSE 80

CMD [ "python", "./manage.py", "runserver", "0.0.0.0:80", "--insecure"]
