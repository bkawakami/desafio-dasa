from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^read', views.DocumentationView.as_view(), name='documentation-index'),
]