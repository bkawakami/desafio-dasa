# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.template import loader
from django.views import View
import os


class DocumentationView(View):
    def get(self, request):
        template = loader.get_template('documentation.html')
        module_dir = os.path.dirname(__file__)
        file_path = os.path.join(module_dir, 'markdown/doc.md')
        documentation = open(file_path)
        context = {
            'title': 'Documentação Desafio EXP',
            'the_text': documentation.read()
        }
        return HttpResponse(template.render(context, request))
