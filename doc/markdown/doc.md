# Documentação API Desafio EXP

## GET /doctors/all

Retorna todos médicos

## GET /exams/all

Retorna todos tipos de exames

## GET /rooms/all

Retorna todas salas disponíveis

## GET /rooms/schedule

Retorna todos os agendamentos

## GET /patient/random

Retorna um paciente randomicamente

## GET /patient/random?name={name}

Retorna um paciente baseado em seu nome