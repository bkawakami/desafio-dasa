# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Exam(models.Model):
    name = models.CharField(max_length=512)
    specialty = models.CharField(max_length=512)
    time_for = models.IntegerField()
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name
