# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse
from django.views import View
from django.core import serializers

# Create your views here.
from exams.models import Exam


class ExamsApi(View):
    def get(self, request):
        records = Exam.objects.filter()

        serialized_records = serializers.serialize('python', records,
                                                    fields=['name', 'specialty', 'time_for', 'active'])

        json_records = [d['fields'] for d in serialized_records]

        data = {"status": "success", "records": json_records}

        return JsonResponse(data, status=200)

