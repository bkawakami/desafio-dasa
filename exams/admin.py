# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from exams.models import Exam


class ExamAdmin(admin.ModelAdmin):
    list_display = ['name', 'specialty', 'time_for', 'active']
    fields = ['name', 'specialty', 'time_for', 'active']
    search_fields = ['name', 'specialty', 'time_for', 'active']


admin.site.register(Exam, ExamAdmin)
