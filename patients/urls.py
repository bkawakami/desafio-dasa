from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^random', views.PatientRandomApi.as_view()),
]