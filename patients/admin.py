# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from patients.models import Patient


class PatientAdmin(admin.ModelAdmin):
    list_display = ['name', 'gender', 'is_bad', 'room_schedule']
    fields = ['name', 'gender', 'is_bad', 'room_schedule']
    search_fields = ['name', 'gender', 'is_bad', 'room_schedule']


admin.site.register(Patient, PatientAdmin)
