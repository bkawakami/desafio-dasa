# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import JsonResponse
from django.views import View
from django.core import serializers

from patients.models import Patient

from time import sleep

import random


class PatientRandomApi(View):
    def get(self, request):

        name = request.GET.get('name', '')

        if name != '':
            records = Patient.objects.filter(name__icontains=name)
        else:
            records = Patient.objects.all()

        serialized_records = serializers.serialize('python', [random.choice(records), ],
                                                   fields=['name', 'gender', 'is_bad', 'room_schedule'])

        json_records = [d['fields'] for d in serialized_records]

        times_to_sleep = [1, 2, 3, 4, 5, 6]

        sleep(random.choice(times_to_sleep))

        data = {"status": "success", "records": json_records}

        return JsonResponse(data, status=200)
