# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from rooms.models import RoomSchedule


class Patient(models.Model):
    name = models.CharField(max_length=512)
    gender = models.CharField(max_length=512)
    room_schedule = models.ForeignKey(RoomSchedule, on_delete=models.CASCADE)
    is_bad = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name
