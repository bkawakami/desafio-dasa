from setuptools import setup

setup(
    name='Desafio DASA Labs',
    version='0.0.1',
    packages=[],
    author='Bruno Akio Kawakami',
    author_email='bruno.kawakami@dasa.com.br',
    install_requires=[
        'django==1.11.15',
        'names==0.3.0',
        'django-markdown==0.8.4',
        'django-markup==1.2',
    ]
)
